String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

let getTime = (milli) => {
  let time = new Date(milli);
  let days = time.getUTCDay();
  let hours = time.getUTCHours();
  let minutes = time.getUTCMinutes();
  let seconds = time.getUTCSeconds();
  let milliseconds = time.getUTCMilliseconds();
  if (days > 0){
  	return days+" days ago";
  }
  if (hours > 0){
  	return hours+" hours ago"
  }
  if (minutes > 0){
  	return minutes+" minutes ago"
  }

  return days+ ":"+hours + ":" + minutes + ":" + seconds + ":" + milliseconds
  
};

function get_list_view_entries_from_JSON (DOM_node,JSON_text, post_error){

	var newRow = [];
	// if (post_error == true) {
	// 	return_text = '<ul data-role="listview" data-inset="true" data-count-theme="a">'
	// }else {
	// 	return_text = '<ul data-role="listview" data-inset="true" data-count-theme="a">'
	// }
	
	$.each(JSON_text,function(i,el)
	{
		//return_text = return_text + "\n  "+el.id+' - '+el.text+ ' - '+el.username+ ' - '+el.rating+ ' - '+el.username+ ' - '+el.post_type_id+ ' - '+el.replies+ ' - '+el.date_created;
		var return_text = "";
		var dateOfPost = el.date_created;
		dateOfPost = dateOfPost.replaceAll("Z", "");
		dateOfPost = dateOfPost.replaceAll("T", " ");
		//dateOfPost = dateOfPost.replace("T", " ");
		dateOfPost = dateOfPost.replaceAll("-", " ");
		//alert (dateOfPost);
		dateOfPost = Date.parse(dateOfPost);
		dateNow = Date.now();
		
		var timeElapsed =  getTime(dateNow - dateOfPost);
		
		return_text = return_text + '<li id='+el.id+'><a href="#individual_comment"><p>'+el.username+'</p>';
		return_text = return_text + '<p><strong>'+el.text+'</strong></p>';
		return_text = return_text + '<p class="ui-li-aside"><strong>'+timeElapsed+'</strong></p>'
		if (el.replies != null){
			return_text = return_text + '<p>'+el.replies+' replies</p>'	
		}
		else {
			return_text = return_text + '<p>0 replies</p>'		
		}
		
		return_text = return_text + '</a></li>';
		newRow.push(return_text);
	});

	
	// return_text.push('<li><a href="index.html">    <h2>Stephen Weber</h2>     <p><strong>You\'ve been invited to a meeting at Filament Group in Boston, MA</strong></p>     <p>Hey Stephen, if you\'re available at 10am tomorrow, we\'ve got a meeting with the jQuery team.</p>         <p class="ui-li-aside"><strong>6:24</strong>PM</p>     </a></li>');     
	// return_text.push('<li><a href="index.html">     <h2>jQuery Team</h2>     <p><strong>Boston Conference Planning</strong></p>     <p>In preparation for the upcoming conference in Boston, we need to start gathering a list of sponsors and speakers.</p>        <p class="ui-li-aside"><strong>9:18</strong>AM</p>     </a></li>');
	// return_text.push('<li><a href="index.html">    <h2>Avery Walker</h2>     <p><strong>Re: Dinner Tonight</strong></p>    <p>Sure, let\'s plan on meeting at Highland Kitchen at 8:00 tonight. Can\'t wait!</p>         <p class="ui-li-aside"><strong>4:48</strong>PM</p>    </a></li>');     
		

	DOM_node.append (newRow);
	DOM_node.listview('refresh');
	//return return_text;

	
};

var persistent_object = {};
$(document).on('pagecontainershow', function (evt,ui) {
			persistent_object = {'userid':2};
            var pageId = ui.toPage[0].id;
            if (pageId == "business"){
            	//alert ("this is the business page");

            }
            else if (pageId == "home"){
            	//alert ("this is home page");
            	var output = $("#listOfPosts");   
				
				$.ajax({
				    url: 'http://ec2-18-220-83-221.us-east-2.compute.amazonaws.com:3000/posts',
				    //dataType: 'json',
				    //jsonp: 'jsoncallback',
				    timeout: 5000,
				    success: function(data, status){
				    	//$("#listOfPosts").append("data");
				    	var tempText = get_list_view_entries_from_JSON (output, data, false)
				    	
						//output.text(tempText);
      					//output.text(JSON.stringify(data));
				    	//output.listview('refresh');
    
				        
				    },
				    error: function(request, error){
				    	//var JSON_text = '[{"id":1,"text":"Where can I get the best IPA? ","location":"42.335870, -71.091465","post_type_id":2,"rating":0,"date_created":"2017-08-03T08:59:05.000Z","user_id":2,"username":"mansoorpervaiz","replies":4},{"id":2,"text":"new pizza joint on hungtington ave","location":"42.335870, -71.1","post_type_id":0,"rating":21,"date_created":"2017-08-09T20:08:37.000Z","user_id":2,"username":"mansoorpervaiz","replies":2},{"id":3,"text":"where can I buy a good coffee ginder?","location":"42.535870, -71.091465","post_type_id":1,"rating":9,"date_created":"2017-08-09T20:08:37.000Z","user_id":3,"username":"fahadp","replies":null}]'
				        output.text(JSON.stringify(request));
				        //output.text(get_list_view_entries_from_JSON(JSON_text, true));

				    }
				});

            	
            }
            
           
 });

function testFunction () {

	
	var type_of_post = document.getElementById("#post_text");
	type_of_post_value = JSON.stringify(type_of_post);

	//alert ("the test function is called: "+type_of_post_value);//+persistent_object.userid);
};

$( ".new_post_button" ).bind( "click", function(event, ui) {
  alert ("the test function is called: "+type_of_post_value);
});




