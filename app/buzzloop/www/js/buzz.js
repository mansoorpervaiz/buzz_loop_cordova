var latidue;
var longitude;

var storage;

function init() {

	document.addEventListener ("deviceready", onDeviceReady, false);
	storage = window.localStorage;

}

function onDeviceReady () {

	var node = document.createElement('link');
	node.setAttribute ('rel', 'stylesheet');
	node.setAttribute ('type', 'text/css');

	if (cordova.platformI == 'ios'){
		node.setAttribute('href', 'buzz_ios.css');
		window.StatusBar.overlaysWebView(false);
		window.StatusBar.styleDefault();
	}
	else {
		node.setAttribute('href', 'buzz_android.css');
		window.StatusBar.backgroundColorByHexString('#bc6619');
	}
	document.getElementsByTagName('head')[0].appendChild(node);

	setMyCurrentLocation ();

}

function setMyCurrentLocation () {
	navigator.geolocation.getCurrentPosition (setCurrentLocation, locationError, {enableHighAccuracy:true});
}

function setCurrentLocation (position) {
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	storage.setItem('currentLocationLatitude', latitude);
	storage.setItem('currentLocationLongitude', longitude);
	navigator.notification.alert("Success:"+latitude+",\n"+longitude);

}

function locationError (error) {
	navigator.notification.alert("Error code: "+erro.code + "\nError Message: "+error.message);
}

function goToHomeScreen () {
	navigator.notification.alert ("you tapped on home screen");

}




