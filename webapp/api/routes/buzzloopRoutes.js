'use strict';
module.exports = function(app) {
  var buzzloop = require('../controllers/buzzloopController');


  // buzzloop Routes
  app.route('/')
  	.get(buzzloop.blah);

  app.route('/users')
    .get(buzzloop.list_all_users)
    .post(buzzloop.create_a_user);


  app.route('/users/:userId')
    .get(buzzloop.read_a_user)
    .put(buzzloop.update_a_user)
    .delete(buzzloop.delete_a_user);
};