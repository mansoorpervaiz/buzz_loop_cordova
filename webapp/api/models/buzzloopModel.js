'user strict';
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var users = new Schema ({

	username: {
		type: String, 
		Required: 'Kindly enter username'
	},
	password: {
		type: String, 
		Required: 'Kindly enter password'
	},
	Created_date: {
		type: Date,
		default: Date.now
	}


});

module.exports = mongoose.model ('Users', users);
